# justwork content server

## To start server:
1) go to ../justwork folder
2) run console command: ```sudo docker build --file Dockerfile_content_server_api -t "content-server-api:latest" .```
3) make sure that host redis server (port 6379) and postgreSQL server (5432) are stopped
4) run tests using console command: ```sudo docker-compose up autotests```
5) run server using console command: ```sudo docker-compose up runserver```

## API examples:
GET /pages - returns all pages content (with pagination)  
GET /pages/pk - returns detailed content for page with some pk

## TODO
1) Audio DB should have file field?
2) add comments
3) use swagger or something to generate API documentation
