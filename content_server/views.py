from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.settings import api_settings

from content_server.models import Page
from content_server.serializers import DetailedPageSerializer, AllPagesSerializer


class PagesViewSet(viewsets.ViewSet):
    def list(self, request):
        pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
        paginator = pagination_class()
        queryset = Page.objects.all()

        page = paginator.paginate_queryset(queryset, request)
        serializer_class = AllPagesSerializer(
            page, many=True, context={'request': request})

        return paginator.get_paginated_response(serializer_class.data)

    def retrieve(self, request, pk=None):
        queryset = Page.objects.all()
        page = get_object_or_404(queryset, pk=pk)
        serializer = DetailedPageSerializer(page, context={'request': request})

        return Response(serializer.data)
