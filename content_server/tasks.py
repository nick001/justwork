import logging
from django.apps import apps
from django.db.models import F

from content_server.apps import ContentServerConfig
from content_server.celery import app

log = logging.getLogger('yourapp')


@app.task
def page_save_task(model_name, pk, new_title, new_content, *args, **kwargs):
    model = apps.get_model(ContentServerConfig.name, model_name=model_name)
    page = model.objects.get(pk=pk)
    page._content = {}
    page.title = new_title

    for entry_name, data in new_content.items():

        if 'content_type' not in data.keys():
            continue

        if 'pk' not in data.keys():
            continue

        model_name = data['content_type']
        pk = data['pk']

        try:
            model = apps.get_model(
                ContentServerConfig.name, model_name=model_name)
        except LookupError:
            continue

        entry = model.objects.filter(pk=pk)
        if not entry:
            continue

        page._content[entry_name] = data

    super(model, page).save(*args, **kwargs)


@app.task
def increase_counter(model_name, pk):
    model = apps.get_model(ContentServerConfig.name, model_name=model_name)
    entry = model.objects.get(pk=pk)
    entry.counter = F('counter') + 1
    entry.save()
