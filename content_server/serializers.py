from rest_framework import serializers

from content_server.models import Page


class DetailedPageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Page
        fields = ['url', 'content']


class AllPagesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Page
        fields = ['title', 'url', '_content']
