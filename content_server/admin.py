from django.contrib import admin

from content_server.models import Page, Video, Audio, Text


@admin.register(Page, Video, Audio, Text)
class AdminTitleSearch(admin.ModelAdmin):
    search_fields = ['title']
