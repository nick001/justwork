from collections import OrderedDict

from django.apps import apps
from django.contrib.postgres.fields import JSONField
from django.db import models

from content_server.apps import ContentServerConfig
from content_server.tasks import page_save_task, increase_counter


class TitleMixin(models.Model):
    title = models.CharField(max_length=100, default='', blank=True)

    class Meta:
        abstract = True


class CounterMixin(models.Model):
    counter = models.IntegerField(default=0)

    class Meta:
        abstract = True


class DetailsMixin(object):
    def details(self):
        details_dict = dict(self.__dict__)
        del details_dict['_state']

        return details_dict


class Page(TitleMixin, models.Model):
    _content = JSONField()

    @property
    def content(self):
        detailed_content = OrderedDict()
        for entry_name, data in self._content.items():
            model_name = data['content_type']
            pk = data['pk']
            model = apps.get_model(
                ContentServerConfig.name, model_name=model_name)
            entry = model.objects.get(pk=pk)
            detailed_content[entry_name] = entry.details()

            increase_counter.delay(model_name, pk)

        return detailed_content

    def save(self, *args, **kwargs):
        page_save_task.delay(
            self.__class__.__name__, self.pk, self.title, self._content,
            *args, **kwargs
        )


class Video(DetailsMixin, TitleMixin, CounterMixin, models.Model):
    video_file = models.FileField(upload_to='video/')
    subtitles_file = models.FileField(upload_to='subtitle/')


class Audio(DetailsMixin, TitleMixin, CounterMixin, models.Model):
    bitrate = models.IntegerField()


class Text(DetailsMixin, TitleMixin, CounterMixin, models.Model):
    text = models.TextField()
