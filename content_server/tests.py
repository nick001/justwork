from django.test import TestCase
from content_server.models import Page
from django.test import Client


class PagesTestCase(TestCase):
    fixtures = ['initial_data_content']

    def test_pages_loaded(self):
        pages = Page.objects.all()

        self.assertEqual(len(pages), 6)

    def test_all_pages_first_page_content(self):
        client = Client()
        response = client.get('/pages')

        first_page_result = {
            'title': '1 page title',
            'url': 'http://testserver/pages/1',
            '_content': {
                '1': {'pk': 1, 'content_type': 'Audio'},
                '2': {'pk': 1, 'content_type': 'Video'}
            }
        }

        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.json()['count'], 6)
        self.assertEquals(response.json()['results'][0], first_page_result)

    def test_detailed_content_first_page(self):
        client = Client()
        response = client.get('/pages/1')

        first_page_result_detailed = {
            'url': 'http://testserver/pages/1',
            'content': {
                '1': {
                    'id': 1,
                    'title': '1 title audio',
                    'counter': 0,
                    'bitrate': 240
                },
                '2': {
                    'id': 1,
                    'title': 'First video title',
                    'counter': 0,
                    'video_file': 'video/flame_Nc7FpAh.avi',
                    'subtitles_file': 'subtitle/1_Epneo0o.srt'
                }
            }
        }

        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.json(), first_page_result_detailed)
