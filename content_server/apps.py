from django.apps import AppConfig


class ContentServerConfig(AppConfig):
    name = 'content_server'
